package classifyFunctions

import (
	"fmt"
	"image"
	"time"

	"gitlab.com/okotek/okotypes"
	types "gitlab.com/okotek/okotypes"

	"gocv.io/x/gocv"
)

//SerialClassify classifies serially
func _SerialClassify(input chan types.Frame, output chan types.Frame, classifier gocv.CascadeClassifier) {

	//My channels got a little knotted here
	//toPauser := make(chan types.Frame)
	//go pauseFunc(toPauser, output)
	var detections []image.Rectangle

	for iter := range input {

		matImg := types.CreateMatFromFrameSimp(iter)
		detections = classifier.DetectMultiScale(matImg)
		// #####################################################################################################
		// This is the actual importaint object detection stuff right here
		// #####################################################################################################
		if len(detections) < 1 {
			continue
			fmt.Println("Dumping Data")
		} else {
			output <- iter
			fmt.Println("Sending to Pauser")
		}
	}

}

//SerialClassify classifies serially
func SerialClassify(input chan types.Frame, output chan types.Frame, classifier gocv.CascadeClassifier, classifierTag string) {

	//My channels got a little knotted here
	//toPauser := make(chan types.Frame)
	//go pauseFunc(toPauser, output)
	var detections []image.Rectangle
	var cTagList okotypes.TagList
	cTagList.ClassName = classifierTag

	for iter := range input {

		matImg := types.CreateMatFromFrameSimp(iter)
		detections = classifier.DetectMultiScale(matImg)
		// #####################################################################################################
		// This is the actual importaint object detection stuff right here
		// #####################################################################################################
		if len(detections) < 1 {
			continue
			fmt.Println("Dumping Data")
		} else {
			cTagList.Rects = detections
			iter.ClassifierTags = append(iter.ClassifierTags, cTagList)
			output <- iter
			fmt.Println("Sending to Pauser")
			cTagList.Rects = []image.Rectangle{}
		}
	}

}

func pauseFunc(input chan types.Frame, output chan types.Frame) {

	var userDiffs map[string]time.Time
	var tDiff time.Duration
	var pauseDelay time.Duration = time.Second * 1

	for iter := range input {
		fmt.Println("Iterating")
		if lTime, ok := userDiffs[iter.UserName]; ok {
			tDiff = time.Now().Sub(lTime)
			if tDiff > pauseDelay {
				output <- iter
				userDiffs[iter.UserName] = time.Now()
				fmt.Println("Not dumping Frame")
			} else {
				fmt.Println("Dumping Frame")
				continue
			}
		} else {
			userDiffs[iter.UserName] = time.Now()
		}

	}
}
